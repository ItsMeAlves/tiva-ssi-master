/*********************************************************************************
 *																				 *
 * freertos_demo.c - Simple FreeRTOS example.									 *
 *																				 *
 * Copyright (c) 2012-2017 Texas Instruments Incorporated.  All rights reserved. *
 * Software License Agreement													 *
 * 																				 *
 * Texas Instruments (TI) is supplying this software for use solely and			 *
 * exclusively on TI's microcontroller products. The software is owned by		 *
 * TI and/or its suppliers, and is protected under applicable copyright			 *
 * laws. You may not combine this software with "viral" open-source				 *
 * software in order to form a larger program.									 *
 *																				 *
 * THIS SOFTWARE IS PROVIDED "AS IS" AND WITH ALL FAULTS.						 *
 * NO WARRANTIES, WHETHER EXPRESS, IMPLIED OR STATUTORY, INCLUDING, BUT			 *
 * NOT LIMITED TO, IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR		 *
 * A PARTICULAR PURPOSE APPLY TO THIS SOFTWARE. TI SHALL NOT, UNDER ANY			 *
 * CIRCUMSTANCES, BE LIABLE FOR SPECIAL, INCIDENTAL, OR CONSEQUENTIAL			 *
 * DAMAGES, FOR ANY REASON WHATSOEVER.											 *
 *																				 *
 * This is part of revision 2.1.4.178 of the EK-TM4C123GXL Firmware Package.	 *
 *																				 *
 *********************************************************************************/

#include <stdbool.h>
#include <stdint.h>
#include "inc/hw_memmap.h"
#include "inc/hw_types.h"
#include "inc/hw_gpio.h"
#include "driverlib/gpio.h"
#include "driverlib/pin_map.h"
#include "driverlib/rom.h"
#include "driverlib/sysctl.h"
#include "driverlib/uart.h"
#include "driverlib/flash.h"
#include "driverlib/adc.h"
#include "driverlib/ssi.h"
#include "drivers/buttons.h"
#include "utils/uartstdio.h"

#define OFF 0x00
#define RED 0x02
#define BLUE 0x04
#define GREEN 0x08

/******************************************************************************
 * Configure the UART and its pins.  This must be called before UARTprintf(). *
 ******************************************************************************/
void ConfigureUART(void)
{
    // Enable the GPIO Peripheral used by the UART.
    ROM_SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOA);

    // Enable UART0
    ROM_SysCtlPeripheralEnable(SYSCTL_PERIPH_UART0);

    // Configure GPIO Pins for UART mode.
    ROM_GPIOPinConfigure(GPIO_PA0_U0RX);
    ROM_GPIOPinConfigure(GPIO_PA1_U0TX);
    ROM_GPIOPinTypeUART(GPIO_PORTA_BASE, GPIO_PIN_0 | GPIO_PIN_1);

    // Use the internal 16MHz oscillator as the UART clock source.
    UARTClockSourceSet(UART0_BASE, UART_CLOCK_PIOSC);

    // Initialize the UART for console I/O.
    UARTStdioConfig(0, 115200, 16000000);
}

/***************************************
 * Main function, program entry point. *
 ***************************************/
int main(void)
{
	uint8_t ui8CurButtonState = 0, ui8PrevButtonState = 0;
	uint32_t ui32Message = 0;

    // Set the clocking to run at 50 MHz from the PLL.
    ROM_SysCtlClockSet(SYSCTL_SYSDIV_4 | SYSCTL_USE_PLL | SYSCTL_XTAL_16MHZ |
                       SYSCTL_OSC_MAIN);

    HWREG(GPIO_PORTF_BASE + GPIO_O_LOCK) = GPIO_LOCK_KEY;
    HWREG(GPIO_PORTF_BASE + GPIO_O_CR) = 0xFF;

    SysCtlDelay(20000000);
    ButtonsInit();
    SysCtlDelay(20000000);

    SysCtlPeripheralEnable(SYSCTL_PERIPH_SSI0);
    SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOA);
    GPIOPinConfigure(GPIO_PA2_SSI0CLK);
    GPIOPinConfigure(GPIO_PA3_SSI0FSS);
    GPIOPinConfigure(GPIO_PA5_SSI0TX);
    GPIOPinTypeSSI(GPIO_PORTA_BASE, GPIO_PIN_5 | GPIO_PIN_3 | GPIO_PIN_2);
    SysCtlDelay(20000000);

    SSIConfigSetExpClk(SSI0_BASE, SysCtlClockGet(), SSI_FRF_MOTO_MODE_0, SSI_MODE_MASTER, 10000, 16);
    SSIEnable(SSI0_BASE);

    // Initialize the UART and configure it
    ConfigureUART();
    UARTprintf("\n*******************\n* SSI master logs *\n*******************\n");

    while(1)
    {
    	ui8CurButtonState = ButtonsPoll(0, 0);
    	if(ui8CurButtonState != ui8PrevButtonState)
    	{
    		ui8PrevButtonState = ui8CurButtonState;
    		if((ui8CurButtonState & ALL_BUTTONS) != 0)
    		{
    			if((ui8CurButtonState & ALL_BUTTONS) == LEFT_BUTTON) {
    				ui32Message = LEFT_BUTTON;
    				UARTprintf("LEFT pressed\n");
    			}
    			else if((ui8CurButtonState & ALL_BUTTONS) == RIGHT_BUTTON) {
    				ui32Message = RIGHT_BUTTON;
    				UARTprintf("RIGHT pressed\n");
    			}
    			else
    				ui32Message = 0x00;
    		}
    		else
    			ui32Message = 0x00;
    	}
    	else
    		ui32Message = 0x00;

    	SSIDataPut(SSI0_BASE, ui32Message);
    	while(SSIBusy(SSI0_BASE));
    }
}

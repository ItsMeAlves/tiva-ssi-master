# Tiva C Launchpad SSI Comm - Master
For slave code, please refer to [its repository](https://gitlab.com/ItsMeAlves/tiva-ssi-slave).
<br>
<br>
It sends through the SSI peripheral a message that represents a button event.
* When the LEFT_BUTTON is pressed, it sends the LEFT_BUTTON value.
* When the RIGHT_BUTTON is pressed, it sends the RIGHT_BUTTON value.

## Board wiring
It's only needed to connect the SSI pins when the board has a common electrical reference.
The configured pins are:

Master | Slave
------ | ------
CLOCK (PA2) | CLOCK (PA2)
FSS (PA3) | FSS (PA3)
TX (PA5) | RX (PA4)
